pyTelegramBotAPI==3.7.3
uwsgi==2.0.19.1
gunicorn==20.0.4
paste==3.5.0
six==1.4.0
setuptools==45.2.0
falcon==2.0.0
schedule==0.6.0