import json
import os
import sqlite3
import time

import schedule
import telebot


channel = os.getenv('CHANNEL_ID')
bot = telebot.TeleBot(os.getenv('TELEGRAM_TOKEN'))


def check_status():
    connection = sqlite3.connect("app/sqlite/database.db")
    cursor = connection.cursor()
    dead_terminals = cursor.execute("""
        select
            *
        from terminal
        where time(datetime, '+10 minutes', 'localtime') < current_time
        and time(datetime, '+30 minutes', 'localtime') > current_time""").fetchall()
    if dead_terminals:
        for terminal in dead_terminals:
            msg = 'С терминалом {} что-то случилось! Последнее время, когда терминал отвечал: {}'.format(
                terminal[0],
                terminal[1]
            )
            bot.send_message(channel, msg)

    cursor.close()
    connection.close()


schedule.every(30).minutes.do(check_status)
while True:
    schedule.run_pending()
    time.sleep(1)
