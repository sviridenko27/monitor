from .server import Server
import falcon


server = Server()
app = falcon.API()
app.add_route('/alive', server)
