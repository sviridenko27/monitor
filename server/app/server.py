import json
import sqlite3

import falcon


class Server:
    def __init__(self):
        self.connection_string = "app/sqlite/database.db"
        self.terminals = ['TSUM']
        self._init_terminals()

    def on_post(self, request, response):
        name = json.load(request.stream).get('name', False)
        if not name:
            response.status, response.body = falcon.HTTP_400, '{"msg": "Failed"}'
        else:
            self._update_status(name)
            response.status, response.body = falcon.HTTP_200, '{"msg": "Success"}'

    def on_get(self, request, response):
        try:
            connection = sqlite3.connect(self.connection_string)
            cursor = connection.cursor()
            terminals = cursor.execute("select * from terminal;").fetchall()
            cursor.close()
        except sqlite3.Error as e:
            print("No one terminal was found! Error: ", e, flush=True)
        finally:
            response.status, response.body = falcon.HTTP_200, '{"msg": "' + str(terminals) + '"}'
            if connection:
                connection.close()

    def _update_status(self, name):
        if name in self.terminals:
            self.execute("update terminal set datetime = datetime('now', 'localtime') where name = '{}';".format(name))
        else:
            raise Exception('Отсутствует имя терминала!')

    def _init_terminals(self):
        try:
            connection = sqlite3.connect(self.connection_string)
            cursor = connection.cursor()
            is_table = cursor.execute("select name from sqlite_master where type='table' and name='terminal';").fetchall()
            if not is_table:
                cursor.execute("create table terminal(name text, datetime datetime);")

            for terminal in self.terminals:
                is_terminal = cursor.execute("select * from terminal where name = '{}';".format(terminal)).fetchall()
                if not is_terminal:
                    cursor.execute("insert into terminal values ('{}', datetime('now', 'localtime'));".format(terminal))

            connection.commit()
            cursor.close()
        except sqlite3.Error as e:
            print("Operation was failed. Error: ", e, flush=True)
        finally:
            if connection:
                connection.close()

    def execute(self, query):
        try:
            connection = sqlite3.connect(self.connection_string)
            cursor = connection.cursor()
            cursor.execute(query)
            connection.commit()
            cursor.close()
        except sqlite3.Error as e:
            print("Operation was failed. Error: ", e, flush=True)
        finally:
            if connection:
                connection.close()
